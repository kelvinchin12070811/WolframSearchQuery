let version = "1.0.0";
let query = prompt(`Wolfram|Alpha Search Query v${version}\nEnter your query:`, "");
if (query != "")
{
    let wolfram = "https://www.wolframalpha.com/input/?i=";
    location.href = wolfram + query.replace(/ /g, "+");
}