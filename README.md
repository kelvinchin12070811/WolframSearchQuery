# Wolfram|Alpha Search Query
Wolfram|Alpha Search Query is a simple experiment of the bookmarklet

This bookmarklet allow user to quickly search from Wolfram|Alpha

## Usage
Download lates html file from [here](https://bitbucket.org/kelvinchin12070811/wolframsearchquery/downloads)
and add the link inside the file to bookmark

Click on the bookmark item to launch.