#!/usr/bin/env ruby
message = "Add the following link to your book mark:<br><a href='%s'>Wolfram|Alpha Search Query</a>"
output = "javascript:(function(){%s})()"
buffer = ""

File.foreach("query.js") do |line|
    buffer += line.strip
end

buffer = output % [buffer]
buffer = message % [buffer]

output = File.open("install.html", "w")
output.puts "<!DOCTYPE html><html><body>"
output.puts buffer
output.puts "</body></html>"